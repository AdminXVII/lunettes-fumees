with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "env";
  buildInputs = [
    binutils.bintools pkgconfig gtk3 xorg.libX11
      alsaLib
      cmake
      freetype
      expat
      openssl
      pkgconfig
      python3
      vulkan-validation-layers
      xlibs.libX11
  ];

  APPEND_LIBRARY_PATH = stdenv.lib.makeLibraryPath [
    vulkan-loader
    xlibs.libXcursor
    xlibs.libXi
    xlibs.libXrandr
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$APPEND_LIBRARY_PATH"
    export RUSTFLAGS="-C target-cpu=native"
  '';
}
