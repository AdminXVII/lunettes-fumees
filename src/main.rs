use std::time::Instant;
use winit::{
    event::{ElementState, Event, KeyboardInput, StartCause, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::Window,
};

mod window;
use window::Hider;

const MSG_SMALL: &'static str = "Look away";
const MSG_BIG: &'static str = "Take a break";

fn main() {
    let lookaway_duration = humantime::parse_duration("20s").expect("Could not parse duration");
    let break_duration = humantime::parse_duration("5m").expect("Could not parse duration");
    let cycle_duration = humantime::parse_duration("20m").expect("Could not parse duration");
    let num_lookaway = 3;

    let event_loop = EventLoop::new();
    // Hacky at best, but the only way to list monitors inside the event loop is to have access
    //   to a window, so this is a hidden window that is kept around only for this purpose.
    // It sucks, but the issue was raised and I'm waiting for feedback
    let ref_window = Window::new(&event_loop).expect("Could not create dummy window");
    ref_window.set_visible(false);
    let mut next_update = Instant::now();
    let mut state = Some(Hider::new(
        &event_loop,
        ref_window.available_monitors(),
        MSG_SMALL,
        next_update + lookaway_duration,
    ));

    // Render loop
    let mut update_no = 0u8;
    event_loop.run(move |event, event_loop, control_flow| {
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: _state,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    },
                ..
            } => {
                if let Some(state) = &mut state {
                    state.press(_state == ElementState::Pressed);
                }
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                window_id,
            } => {
                if let Some(state) = &mut state {
                    state.resize(new_size, window_id);
                }
            }
            Event::MainEventsCleared => {
                // Queue a RedrawRequested event.
                if let Some(state) = &state {
                    state.request_redraw();
                }
            }
            Event::RedrawRequested(id) => {
                if let Some(state) = &mut state {
                    state.redraw(id);
                }
            }
            Event::NewEvents(StartCause::ResumeTimeReached { .. }) => {
                if let Some(state) = &mut state {
                    state.update();
                } else {
                    update_no = (update_no + 1) % num_lookaway;
                    let msg = if update_no == 0 { MSG_BIG } else { MSG_SMALL };
                    let target = if update_no == 0 {
                        break_duration
                    } else {
                        lookaway_duration
                    };
                    state = Some(Hider::new(
                        event_loop,
                        ref_window.available_monitors(),
                        msg,
                        next_update + target,
                    ));
                    next_update += cycle_duration;
                }
            }
            _ => {}
        }
        let next = if let Some(state) = state.as_ref().filter(|s| !s.should_quit()) {
            state.next_update()
        } else {
            state = None;
            next_update
        };
        *control_flow = ControlFlow::WaitUntil(next);
    });
}
