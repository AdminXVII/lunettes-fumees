use std::time::{Duration, Instant};
use wgpu_glyph::{
    ab_glyph::{FontRef},
    GlyphBrush, GlyphBrushBuilder, HorizontalAlign, Layout, Section, Text, VerticalAlign,
};
use winit::{
    dpi::PhysicalSize,
    event_loop::EventLoopWindowTarget,
    monitor::MonitorHandle,
    platform::unix::WindowExtUnix,
    window::{Fullscreen, Window, WindowBuilder, WindowId},
};

const EXIT_TARGET: u8 = 50;
const INC_DELAY: u64 = 100;

fn rgb_to_wgpu(color: u32) -> [f32; 4] {
    let color = color.to_be_bytes();

    [
        (color[0] as f32 / 255.).powf(2.2),
        (color[1] as f32 / 255.).powf(2.2),
        (color[2] as f32 / 255.).powf(2.2),
        (color[3] as f32 / 255.),
    ]
}

#[derive(Debug)]
pub struct Hider {
    windows: Vec<InnerWindow>,
    exit_counter: u8,
    initialized: bool,
    pressed: bool,
    target: Instant,
    next_update: Instant,
}

#[derive(Debug)]
pub struct InnerWindow {
    window: Window,
    surface: wgpu::Surface,
    glyph_brush: GlyphBrush<(), FontRef<'static>>,
    text: &'static str,
    swap_chain: wgpu::SwapChain,
    queue: wgpu::Queue,
    device: wgpu::Device,
}

impl Hider {
    pub fn new<T, I: IntoIterator<Item = MonitorHandle>>(
        event_loop: &EventLoopWindowTarget<T>,
        monitors: I,
        msg: &'static str,
        target: Instant,
    ) -> Self {
        Self {
            windows: monitors
                .into_iter()
                .map(|monitor| InnerWindow::new(&event_loop, monitor, msg))
                .collect(),
            exit_counter: 0,
            initialized: false,
            pressed: false,
            next_update: Instant::now() + Duration::from_millis(INC_DELAY),
            target,
        }
    }

    pub fn request_redraw(&self) {
        for w in &self.windows {
            w.window.request_redraw();
        }
    }

    pub fn resize(&mut self, size: PhysicalSize<u32>, id: WindowId) {
        if let Some(w) = self.windows.iter_mut().find(|w| w.id() == id) {
            w.resize(size);
        }
    }

    pub fn press(&mut self, pressed: bool) {
        self.pressed = pressed;
    }

    pub fn update(&mut self) {
        if self.pressed {
            self.exit_counter += 1;
        } else {
            self.exit_counter = self.exit_counter.saturating_sub(1);
        }

        self.next_update += Duration::from_millis(INC_DELAY);
    }

    pub fn next_update(&self) -> Instant {
        self.next_update
    }

    pub fn redraw(&mut self, id: WindowId) {
        if let Some((i, w)) = self
            .windows
            .iter_mut()
            .enumerate()
            .find(|(_, w)| w.id() == id)
        {
            w.draw(self.exit_counter, self.target);

            if !self.initialized && i == 0 {
                self.initialized = w.grab();
            }
        }
    }

    pub fn should_quit(&self) -> bool {
        self.exit_counter >= EXIT_TARGET || self.target <= Instant::now()
    }
}

impl InnerWindow {
    const FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Bgra8UnormSrgb;

    pub fn new<T>(
        event_loop: &EventLoopWindowTarget<T>,
        monitor: MonitorHandle,
        text: &'static str,
    ) -> Self {
        let window = WindowBuilder::new()
            .with_fullscreen(Some(Fullscreen::Borderless(monitor)))
            .with_title("Lunettes fumées")
            .with_always_on_top(true)
            .build(&event_loop)
            .unwrap();
        let surface = wgpu::Surface::create(&window);

        // Initialize GPU
        let (device, queue) = futures::executor::block_on(async {
            let adapter = wgpu::Adapter::request(
                &wgpu::RequestAdapterOptions {
                    power_preference: wgpu::PowerPreference::HighPerformance,
                    compatible_surface: Some(&surface),
                },
                wgpu::BackendBit::all(),
            )
            .await
            .expect("Request adapter");

            adapter
                .request_device(&wgpu::DeviceDescriptor {
                    extensions: wgpu::Extensions {
                        anisotropic_filtering: false,
                    },
                    limits: wgpu::Limits { max_bind_groups: 1 },
                })
                .await
        });

        // Prepare swap chain
        let size = window.inner_size();

        let swap_chain = device.create_swap_chain(
            &surface,
            &wgpu::SwapChainDescriptor {
                usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
                format: Self::FORMAT,
                width: size.width,
                height: size.height,
                present_mode: wgpu::PresentMode::Mailbox,
            },
        );

        // Prepare glyph_brush
        let inconsolata: &[u8] = include_bytes!("../assets/Inconsolata-Regular.ttf");
        let glyph_brush = GlyphBrushBuilder::using_font(
            FontRef::try_from_slice(inconsolata).expect("Load fonts"),
        )
        .build(&device, Self::FORMAT);
        Self {
            window,
            surface,
            glyph_brush,
            text,
            swap_chain,
            device,
            queue,
        }
    }

    pub fn resize(&mut self, size: PhysicalSize<u32>) {
        self.swap_chain = self.device.create_swap_chain(
            &self.surface,
            &wgpu::SwapChainDescriptor {
                usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
                format: Self::FORMAT,
                width: size.width,
                height: size.height,
                present_mode: wgpu::PresentMode::Mailbox,
            },
        );
    }

    pub fn grab(&self) -> bool {
        self.window.set_cursor_visible(false);
        if let Err(e) = self.window.set_cursor_grab(true) {
            eprintln!("Could not grab cursor: {}", e);
        }
        self.grab_kbd()
    }

    #[cfg(unix)]
    fn grab_kbd(&self) -> bool {
        let display = self.window.xlib_display().expect("No xlib display");
        let x_window = self.window.xlib_window().expect("No xlib window");
        let xlib = x11_dl::xlib::Xlib::open().expect("Could not open XLib connection");
        let out = unsafe {
            (xlib.XUngrabKeyboard)(display as _, x11_dl::xlib::CurrentTime);
            (xlib.XGrabKeyboard)(
                display as _,
                x_window,
                x11_dl::xlib::True,
                x11_dl::xlib::GrabModeAsync,
                x11_dl::xlib::GrabModeAsync,
                x11_dl::xlib::CurrentTime,
            )
        };
        if out != 0 {
            println!("Could not grab the keyboard");
        }
        out == 0
    }

    pub fn id(&self) -> WindowId {
        self.window.id()
    }

    pub fn draw(&mut self, exit_counter: u8, target: Instant) {
        let size = self.window.inner_size();

        // Get a command encoder for the current frame
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Redraw"),
            });

        // Get the next frame
        let frame = self.swap_chain.get_next_texture().expect("Get next frame");

        // Clear frame
        {
            let color = rgb_to_wgpu(0x2E3440FF);

            let _ = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: &frame.view,
                    resolve_target: None,
                    load_op: wgpu::LoadOp::Clear,
                    store_op: wgpu::StoreOp::Store,
                    clear_color: wgpu::Color {
                        r: color[0] as _,
                        g: color[1] as _,
                        b: color[2] as _,
                        a: color[3] as _,
                    },
                }],
                depth_stencil_attachment: None,
            });
        }

        let mut text = format!("Quit progress ({}/{}): ", exit_counter, EXIT_TARGET);
        text.extend((0..exit_counter).into_iter().map(|_| '#'));
        self.glyph_brush.queue(
            Section {
                screen_position: (30.0, 30.0),
                bounds: (size.width as f32, size.height as f32),
                ..Section::default()
            }
            .add_text(
                Text::new(&text)
                    .with_color(rgb_to_wgpu(0xeceff4ff))
                    .with_scale(40.),
            ),
        );
        self.glyph_brush.queue(
            Section {
                bounds: (size.width as f32, size.height as f32),
                screen_position: (size.width as f32 / 2.0, size.height as f32 / 2.0 - 80.),
                layout: Layout::default()
                    .h_align(HorizontalAlign::Center)
                    .v_align(VerticalAlign::Center),
                ..Section::default()
            }
            .add_text(
                Text::new(self.text)
                    .with_color(rgb_to_wgpu(0xbf616aff))
                    .with_scale(180.),
            ),
        );
        let remaining = target.saturating_duration_since(Instant::now());
        let rounded = Duration::from_secs(remaining.as_secs());
        let time = humantime::format_duration(rounded).to_string();
        self.glyph_brush.queue(
            Section {
                bounds: (size.width as f32, size.height as f32),
                screen_position: (size.width as f32 / 2.0, size.height as f32 / 2.0 + 80.),
                layout: Layout::default()
                    .h_align(HorizontalAlign::Center)
                    .v_align(VerticalAlign::Center),
                ..Section::default()
            }
            .add_text(
                Text::new(&time)
                    .with_color(rgb_to_wgpu(0xa3be8cff))
                    .with_scale(160.),
            ),
        );

        // Draw the text!
        self.glyph_brush
            .draw_queued(
                &self.device,
                &mut encoder,
                &frame.view,
                size.width,
                size.height,
            )
            .expect("Draw queued");

        self.queue.submit(&[encoder.finish()]);
    }
}
